package com.ruslan.khasbulatov.site;

/**
 * Enumeration of sites (wipro, google, facebook, etc.)
 *
 * @author Ruslan Khasbulatov
 * @version 1.0
 * @since 12/23/2018
 */
public enum Site {

    WIPRO("http://wiprodigital.com");

    private final String url;

    Site(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return url;
    }

}
