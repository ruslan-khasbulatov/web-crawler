package com.ruslan.khasbulatov.frontier;

import com.ruslan.khasbulatov.site.Site;
import org.jsoup.Jsoup;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Crawler's Frontier, handling sites
 *
 * @author Ruslan Khasbulatov
 * @version 1.0
 * @since 12/23/2018
 */
public final class Frontier {

    private final Site site;
    private final Set<String> links;

    private Frontier(Site site) {
        this.site = site;
        this.links = new HashSet<>();
    }

    public static Frontier of(Site site) {
        return new Frontier(site);
    }

    public void crawl(String url) {
        if (!links.contains(url)) {
            try {
                if (links.add(url)) {
                    System.out.println(url);
                }
                Document document = Jsoup.connect(url).get();
                Elements linksOnPage = document.getElementsByAttributeValueContaining("href", "wiprodigital");
                linksOnPage.parallelStream()
                        .map(page -> page.attr("abs:href"))
                        .filter(attr -> !StringUtil.isBlank(attr) &&
                                !attr.contains("linkedin") && !attr.contains("twitter") &&
                                !attr.contains("instagram") && !attr.contains("tumblr")
                                && !attr.contains("youtube") && !attr.contains("google") &&
                                !attr.contains("facebook"))
                        .forEach(this::crawl);
            } catch (IOException e) {
                System.err.println("For '" + url + "': " + e.getMessage());
            }
        }
    }

}
