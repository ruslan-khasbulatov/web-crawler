package com.ruslan.khasbulatov;

import com.ruslan.khasbulatov.frontier.Frontier;
import com.ruslan.khasbulatov.site.Site;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Web Crawler
 *
 * @author  Ruslan Khasbulatov
 * @version 1.0
 * @since 12/23/2018
 */
public class WebCrawler {

    public static void main(String... args) {
        new WebCrawler().crawl();
    }

    public void crawl() {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        for (Site site : Site.values()) {
            executorService.execute(() -> {
                Frontier frontier = Frontier.of(site);
                frontier.crawl(site.toString());
            });
        }
    }

}
