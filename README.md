# web-crawler project is simple implementation of Web Crawler written in Java Programming Language

1. To compile project, run following command:
   ./gradlew assemble
2. To build project and run all tests, run following command:
   ./gradlew build
3. To clean project, run following command:
   ./gradlew clean

